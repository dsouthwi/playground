# playground

Random testing projects, mostly gitlab integration

```plantuml
@startuml

skinparam nodesep 10
skinparam ranksep 40
card "CMS HCAL Online Software build dependencies"{
component [hcalBase] #chartreuse
together{
[hcalMonVis] #lightgreen
[hcalRBX] #lightgreen
[hcalngRBX] #lightgreen
[hcalSupervisor] #lightgreen
[hcalAux] #lightgreen
[hcalCCM] #lightgreen
[hcalDCC] #lightgreen
[hcalOnlineDB] #lightgreen
}

[hcalAlarm] #technology
[hcalClassic] #technology
[hcalUTCA] #technology
[hcalUHTR] #technology
[hcalHW] #technology
(DCCtool) #technology
[hcalTrig] #honeydew
[hcalCalib] #honeydew
[hcalUpgrade] #white

[hcalBase]<--[hcalngRBX]
[hcalBase]<--[hcalAux]
[hcalBase]<--[hcalDCC]
[hcalBase]<--[hcalMonVis]
[hcalBase]<--[hcalRBX]
[hcalBase]<--[hcalSupervisor]
[hcalBase]<--[hcalCCM]
[hcalBase]<--[hcalOnlineDB]
[hcalRBX]<--[hcalAlarm]
[hcalMonVis]<--[hcalAlarm]
[hcalDCC]<--[hcalHW]
[hcalDCC]<--DCCtool
[hcalngRBX]<--[hcalCalib]
[hcalAux] <--[hcalUTCA]
[hcalAux] <--[hcalUHTR]
[hcalAux] <--[hcalHW]
[hcalAux] <--[hcalClassic]
[hcalAux] <--[hcalUpgrade]
[hcalAux] <--DCCtool
[hcalAux] <--[hcalTrig]
[hcalUTCA]<--[hcalTrig]
[hcalUTCA] <--[hcalUpgrade]
[hcalUTCA]<--[hcalCalib]
[hcalClassic]<--[hcalCalib]
[hcalTrig]<--[hcalUpgrade]
}
@enduml
```
